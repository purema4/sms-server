FROM node:latest

WORKDIR /app

COPY . /app

ENV PORT=3000

EXPOSE 3000

RUN ["npm", "install"]
CMD ["node", "server.js"]

