const express = require('express')
const bodyParser = require('body-parser')
//const util = require('util')
const exec = require('child_process').exec

const accountSid = process.env.ACCOUNT_SID
const authToken = process.env.AUTH_TOKEN
const phoneNumberDest = process.env.PHONE_NUMBER_DEST


if (accountSid == null || authToken == null || phoneNumberDest == null)
{
  process.stdout.write('Unable to start server, ENV variables not configured!')
  process.exit(1);
}



const client = require('twilio')(accountSid, authToken);

const port = process.env.PORT || 3000;


let smscfg = {
  to: phoneNumberDest,
  from: '+18193037926',
}

var app = express()

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

app.get('/', (req, res) => res.send('Hello World!'))

app.post('/sms', (req, res) => {
  console.log(req.body);
  const command = req.body.Body;

  if(req.body.From != phoneNumberDest) {
    res.sendStatus(403)
    return
  }

  res.sendStatus(200);

  exec(command, (error, stdout, stderr) => {
    if (error) {
      console.error(`exec error: ${error}`);
      return;
    }

    client.messages
    .create(Object.assign({
      body: `${stdout}`,
    }, smscfg))
    .then(message => process.stdout.write(message.sid));

  });

})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))